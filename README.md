<b>Group 4</b>:<b>Second Hand Car Price Prediction</b>

To be able to predict used cars market value can help both buyers and sellers. There are lots of individuals who are interested in the used car market at some points in their life because they wanted to sell their car or buy a used car. In this process, it’s a big corner to pay too much or sell less then it’s market value.

In this Project, we are going to predict the Price of Used Cars using various features like  Selling_Price, Kms_Driven, Fuel_Type, Year,transmission etc. The data used in this project is purely of scapred data using octaparse application that use minimal coding to extract data from the various website which for us, we have scraped the data from the https://droom.in/ website which is one of the top indian second hand car selling webiste that  contains clearly all the required features reducing the number of null values.

So after the scaping we have done some data cleaning one the scraped data mainly to removes the erroes, duplicate and unnecessary data so that it makes the module training more accurate and effective.
during model traing we use various algortim for finding the best accury score for each algorithm with which we have the KNN regression having the highest score with traing score 88% and test score 84% which is quite a good performance.
so we use this algoritm as our primary machine learning algoritm and implment for creating the website for predicting the used car price.

## Deployment
for the webiste framework we have use flask for creating a simple website owing to its scalable and flexiblity nature and the pyhton language it use to make it easy to create a one page webiste .

for the deployment we have used the https://www.pythonanywhere.com software to deploy our webiste cause it is easy and provide the flexbility in deploying our website: http://jamyang.pythonanywhere.com/



